# ~/.bash_profile: executed by the command interpreter for login shells.
# This file is read first by bash(1), if ~/.bash_login or ~/.profile
# exists they will not be read.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022


# if running bash
#if [ -n "$BASH_VERSION" ]; then
#    # include .bashrc if it exists
#    if [ -f "$HOME/.bashrc" ]; then
#	. "$HOME/.bashrc"
#    fi
#fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"
# Startup file for login instances of the bash(1) shell.


# First of all, run a .bashrc file if it exists.
test -f ~/.bashrc && . ~/.bashrc

# include machine/Jail/VM specific profile
test -f ~/.bash_profile-`uname -n` && . ~/.bash_profile-`uname -n`
