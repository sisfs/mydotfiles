
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Some ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'

# My most commonly typed commands - shortened
alias ud='du -hd 1'
alias pp='pms.script2 $PWD'
