#!/bin/sh


# add any new dotfiles from this system

## mv all .files to the dotfiles folder and prepare for linking
ls -AF ~|grep ^\\.|grep -v [@/]$|sed 's/\.\(.*\)/cat & >> $PWD\/\1.symlink \&\& rm ~\/&/'|sh

## mv all .directories to the dotfiles folder and prepare for linking
ls -AF ~|grep ^\\.|grep /$|sed 's/\.\(.*\)\//mv & $PWD\/\1.dir/'|sh

## create all the file links
ls|grep .symlink|sed 's/\(.*\).symlink/ln -s $PWD\/& ~\/.\1/'|sh

## create all the directory links
ls|grep .dir|sed 's/\(.*\).dir/ln -s $PWD\/&\/ ~\/.\1/'|sh

git add .
git ci



# can use this file to install certain progs too 
# sudo pkg install nano pv cmdwatch lzop mbuffer 
# etc

# add git projects
# git clone git@gitlab.com:sisfs/sanoid.git
# etc
