# My attempt at dotfile management

Everyone and their brother seems to have a dotfiles repo nowadays. I have been toying around with mine for some time now but just recently found **GNU Stow**; it appears to be the piece I was missing.  
Before I was working on a way to automate the symlinking process. The remnants of those attempts are still in the **create.sh** and **collect.sh** scripts.  
It appears that what I was looking for was `stow --adopt` to bring new files into the fold (after a little _cat herding_ that is).
When I sit down at a new system, the process should be something like this.

```shell
$ [sudo] <pkg|apt|rpm> install stow screen nano git ...
$ cd ~
$ git clone https://gitlab.com/sisfs/mydotfiles.git .dotfiles
$ cd .dotfiles
$ ls
bash    ssh     screen      git     less    zfs     
$ stow bash ssh screen git
```

At this point all my dotfiles should be linked to the home directory or the .folders that they live in. (minus the ones I dont want/need to add, which I can decide now or later).  

If, during the stow process, I receive an error about a file already existing; I can do something like the following to quickly absorb/adopt the files for later inspection.

```shell
$ stow bash
WARNING! stowing bash would cause conflicts:
  * existing target is neither a link nor a directory: .bash_history
All operations aborted.
$ cp -R bash bash-`uname -n`{,-merge}
$ stow --adopt bash-`uname -n`
$ cp -R bash-`uname -n`/ ~
$ cd bash-`uname -n`-merge
$ ls -A|sed 's/.*/cat & >> ~\/&/'|sh
$ cd ..
$ stow --adopt bash-`uname -n`-merge
```

what the above commands do is to make a named copy of the problem stow package, add the contents of the package to the end of the conflicting files and then pull the resultant files into this newly created stow package.  
Prior to that though, we make a copy that just adopts the new files. Then we copy those files back in prep for the merge.  

## The Result

At the end of all of this we have a git repo that we can install on a new machine and pick and choose which dotfiles to copy over. if there are conflicts we can create a new package so that we can, on the fly, choose between the old, new or merged dotfiles for a given package.  



